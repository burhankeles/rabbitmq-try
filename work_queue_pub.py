#!/usr/bin/env python
import pika
import sys
connection = pika.BlockingConnection(
    pika.ConnectionParameters(host='localhost'))
channel = connection.channel()

channel.queue_declare(queue='main_queue', durable=True) # durable=True means that if server stops the published messages stored in queue 
                                                        # (Didn't worked with docker. May need a volume for stored queue, because every restart it creates new container)
                                                        # (check later https://stackoverflow.com/questions/41330514/docker-rabbitmq-persistency)
message = ' '.join(sys.argv[1:]) or "Hello World!"
channel.basic_publish(exchange='',
                      routing_key="main_queue",
                      body=message,
                      properties=pika.BasicProperties(
                         delivery_mode = pika.spec.PERSISTENT_DELIVERY_MODE
                      ))
print(" [x] Sent %r" % message)

connection.close()
