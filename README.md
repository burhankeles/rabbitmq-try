1. Start your server as docker
```docker run -it --rm --name rabbitmq -p 5672:5672 -p 15672:15672 rabbitmq:3.11-management```
2. Install required pika package
```pip3 install pika```
3. If you want to try work queue run following commands (for work_queue_worker.py you can create as much as workers you want):
```python work_queue_worker.py```
4. Copy following codes for testing work queue publisher:
python work_queue_pub.py First message.
python work_queue_pub.py Second message..
python work_queue_pub.py Third message...
python work_queue_pub.py Fourth message....
python work_queue_pub.py Fifth message.....
5. For testing Publish & Subscribe methods do following (for receive_logs_publish_sub.py you can create as much as workers you want):
```python receive_logs_publish_sub.py```
6. Copy the following codes for testing publish& subscribe model:
python emit_log_publish_sub.py 123321
python emit_log_publish_sub.py 23
python emit_log_publish_sub.py 32112
python emit_log_publish_sub.py 44
python emit_log_publish_sub.py 33